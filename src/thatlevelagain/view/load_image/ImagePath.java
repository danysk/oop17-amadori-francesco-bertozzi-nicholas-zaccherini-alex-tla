package thatlevelagain.view.load_image;

/**
 * IMAGE ENUM.
 */
public enum ImagePath {
    /*BACKGROUND*/
    /**
     * 
     */
    BACKGROUND_END_IMAGE("/immagini/sfondi/sfondoEnd.png", 0),
    /**
     * 
     */
    BACKGROUND_HELP_IMAGE("/immagini/sfondi/sfondoHelp.png", 1),
    /**
     * 
     */
    BACKGROUND_LEVEL_IMAGE("/immagini/sfondi/sfondoLevel.png", 2),
    /**
     * 
     */
    BACKGROUND_LOADGAME_IMAGE("/immagini/sfondi/sfondoLoadGame.png", 3),
    /**
     * 
     */
    BACKGROUND_MENU_IMAGE("/immagini/sfondi/sfondoLauncher.png", 4),
    /**
     * 
     */
    BACKGROUND_PAUSE_IMAGE("/immagini/sfondi/sfondoLauncher.png", 5),

    /* BASI */
    /**
     * 
     */
    BASE1("/immagini/basi/base1.png", 6),
    /**
     * 
     */
    BASE5("/immagini/basi/base5.png", 7),
    /**
     * 
     */
    BASE7("/immagini/basi/base7.png", 8),
    /**
     * 
     */
    BASE8("/immagini/basi/base8.png", 9),
    /**
     * 
     */
    BASE12("/immagini/basi/base12.png", 10),

    /*TROPHY*/
    /**
     * 
     */
    TROPHY1X("/immagini/sprite/nonTrovato/formaggioX.png", 11),
    /**
     * 
     */
    TROPHY1V("/immagini/sprite/trovato/formaggioV.png", 12),
    /**
     * 
     */
    TROPHY2X("/immagini/sprite/nonTrovato/vitiX.png", 13),
    /**
     * 
     */
    TROPHY2V("/immagini/sprite/trovato/vitiV.png", 14),
    /**
     * 
     */
    TROPHY3X("/immagini/sprite/nonTrovato/martelloX.png", 15),
    /**
     * 
     */
    TROPHY3V("/immagini/sprite/trovato/martelloV.png", 16),
    /**
     * 
     */
    TROPHY4X("/immagini/sprite/nonTrovato/cacciaviteX.png", 17),
    /**
     * 
     */
    TROPHY4V("/immagini/sprite/trovato/cacciaviteV.png", 18),
    /**
     * 
     */
    TROPHY5X("/immagini/sprite/nonTrovato/chiodoX.png", 19),
    /**
     * 
     */
    TROPHY5V("/immagini/sprite/trovato/chiodoV.png", 20),
    /**
     * 
     */
    TROPHY6X("/immagini/sprite/nonTrovato/ferroX.png", 21),
    /**
     * 
     */
    TROPHY6V("/immagini/sprite/trovato/ferroV.png", 22),
    /**
     * 
     */
    TROPHY7X("/immagini/sprite/nonTrovato/segaX.png", 23),
    /**
     * 
     */
    TROPHY7V("/immagini/sprite/trovato/segaV.png", 24),
    /**
     * 
     */
    TROPHY8X("/immagini/sprite/nonTrovato/cordaX.png", 25),
    /**
     * 
     */
    TROPHY8V("/immagini/sprite/trovato/cordaV.png", 26),
    /**
     * 
     */
    TROPHY9X("/immagini/sprite/nonTrovato/legnoX.png", 27),
    /**
     * 
     */
    TROPHY9V("/immagini/sprite/trovato/legnoV.png", 28),
    /**
     * 
     */
    TROPHY10X("/immagini/sprite/nonTrovato/ingranaggiX.png", 29),
    /**
     * 
     */
    TROPHY10V("/immagini/sprite/trovato/ingranaggiV.png", 30),
    /**
     * 
     */
    TROPHY11X("/immagini/sprite/nonTrovato/collaX.png", 31),
    /**
     * 
     */
    TROPHY11V("/immagini/sprite/trovato/collaV.png", 32),
    /**
     * 
     */
    TROPHY12X("/immagini/sprite/nonTrovato/gancioX.png", 33),
    /**
     * 
     */
    TROPHY12V("/immagini/sprite/trovato/gancioV.png", 34),
    /**
     * 
     */
    ACCALAPPIAGATTI_SINISTRA("/immagini/sprite/generali/mrBean2.png", 35),
    /**
     * 
     */
    BOTTONE("/immagini/sprite/generali/button.png", 36),
    /**
     * 
     */
    GATTO_DESTRA1("/immagini/sprite/generali/player1a.png", 37),
    /**
     * 
     */
    GATTO_DESTRA2("/immagini/sprite/generali/player1b.png", 38),
    /**
     * 
     */
    GATTO_SINISTRA1("/immagini/sprite/generali/player2a.png", 39),
    /**
     * 
     */
    GATTO_SINISTRA2("/immagini/sprite/generali/player2b.png", 40),
    /**
     * 
     */
    CANE_SVEGLIO("/immagini/sprite/generali/caneSveglio.png", 41),
    /**
     * 
     */
    CANE_ADDORMENTATO("/immagini/sprite/generali/caneAddormentato.png", 42),
    /**
     * 
     */
    PORTA_CHIUSA("/immagini/sprite/generali/doorClosed.png", 43),
    /**
     * 
     */
    PORTA_APERTA("/immagini/sprite/generali/doorOpen.png", 44),
    /**
     * 
     */
    END1("/immagini/sprite/trofei/formaggio.png", 45),
    /**
     * 
     */
    END2("/immagini/sprite/trofei/viti.png", 46),
    /**
     * 
     */
    END3("/immagini/sprite/trofei/martello.png", 47),
    /**
     * 
     */
    END4("/immagini/sprite/trofei/cacciavite.png", 48),
    /**
     * 
     */
    END5("/immagini/sprite/trofei/chiodo.png", 49),
    /**
     * 
     */
    END6("/immagini/sprite/trofei/ferro.png", 50),
    /**
     * 
     */
    END7("/immagini/sprite/trofei/sega.png", 51),
    /**
     * 
     */
    END8("/immagini/sprite/trofei/corda.png", 52),
    /**
     * 
     */
    END9("/immagini/sprite/trofei/legno.png", 53),
    /**
     * 
     */
    END10("/immagini/sprite/trofei/ingranaggi.png", 54),
    /**
     * 
     */
    END11("/immagini/sprite/trofei/colla.png", 55),
    /**
     * 
     */
    END12("/immagini/sprite/trofei/gancio.png", 56),
    /**
     * 
     */
    GOMITOLO_TERRA("/immagini/sprite/generali/gomitolo1.png", 57),
    /**
     * 
     */
    GOMITOLO_PIEDI_UNITI("/immagini/sprite/generali/gomitoloPiediUniti.png", 58),
    /**
     * 
     */
    GOMITOLO_CAMMINA("/immagini/sprite/generali/gomitoloCammina.png", 59),
    /**
     * 
     */
    IMPOSTAZIONI("/immagini/sprite/generali/menu.png", 60),
    /**
     * 
     */
    LAMPADINA("/immagini/sprite/generali/lampadina.png", 61),
    /**
     * 
     */
    MATTONE("/immagini/sprite/generali/mattone.png", 62),
    /**
     * 
     */
    ROCCIA("/immagini/sprite/generali/rock.png", 63),
    /**
     * 
     */
    SCOSSA1("/immagini/sprite/generali/scossaVerticale1.png", 64),
    /**
     * 
     */
    SCOSSA2("/immagini/sprite/generali/scossaVerticale2.png", 65),
    /**
     * 
     */
    SPINE1("/immagini/sprite/generali/spine.png", 66),
    /**
     * 
     */
    SPINE2("/immagini/sprite/generali/spineLunghe.png", 67),
    /**
     * 
     */
    SPINE_ALTE("/immagini/sprite/generali/spineAlte.png", 68);

    private final String path;
    private final int position;

    ImagePath(final String pathFile, final int position) {
        this.path = pathFile;
        this.position = position;
    }

    /**
     * @return the String of the path of the file.
     */
    public String getPathImage() {
        return this.path;
    }

    /**
     * @return the position of the image inside the List.
     */
    public int getPosition() {
        return this.position;
    }

}
