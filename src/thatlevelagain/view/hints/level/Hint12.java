package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level12's hint.
 *
 */
public class Hint12 extends HintImpl {

    private static final String NAME = "DON'T TOUCH THE ELECTRIC ZONE";
    private static final String MESSAGE = "DANGER, 220 VOLT!";

    /**
     * 
     */
    public Hint12() {
        super(NAME, MESSAGE);
    }

}

