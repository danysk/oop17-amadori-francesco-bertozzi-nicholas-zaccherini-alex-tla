package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level7's hint.
 *
 */
public class Hint7 extends HintImpl {

    private static final String MESSAGE = "BE CAREFUL!";
    private static final String NAME = "IF YOU ARE CLOSE TO THE BUTTON OR TO THE DOG WHILE IT IS AWAKE, IT WILL BITE YOU!";

    /**
     * 
     */
    public Hint7() {
        super(NAME, MESSAGE);
    }

}
