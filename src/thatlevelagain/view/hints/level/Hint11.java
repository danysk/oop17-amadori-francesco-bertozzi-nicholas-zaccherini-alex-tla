package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level11's hint.
 *
 */
public class Hint11 extends HintImpl {

    private static final String MESSAGE = "DANGER! FALLING EVIL ROCKS!";
    private static final String NAME = "DODGE THE EVIL ROCKS";
    /**
     * 
     */
    public Hint11() {
        super(NAME, MESSAGE);
    }

}

